<!DOCTYPE html>
<html lang="en-us">
<head>
<meta charset="utf-8">
<meta name="generator" content="Hugo 0.15" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.8.0/styles/ir_black.min.css">

<script src="js/highlight.pack.js"</script>
<script>hljs.initHighlightingOnLoad();</script>
<link href="http://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="/css/normalize.css">
<link rel="stylesheet" href="/css/skeleton.css">
<link rel="stylesheet" href="/css/custom.css">
<link rel="alternate" href="/index.xml" type="application/rss+xml" title="A Turn of Phrase">
<title>keystores and certificate expiration - A Turn of Phrase</title>
</head>
<body>

<div class="container">

	<header role="banner">
		<div class="header-logo">
			<a href="/"><img src="https://s.gravatar.com/avatar/76c55292df5e9df9042eaf89ac35954b?s=80" width="60" height="60"></a>
		</div>
		
	</header>


	<main role="main">
		<article itemscope itemtype="http://schema.org/BlogPosting">
			<h1 class="entry-title" itemprop="headline">keystores and certificate expiration</h1>
			<span class="entry-meta"><time itemprop="datePublished" datetime="2015-10-15">October 15, 2015</time></span>
			<section itemprop="entry-text">
				

<p>I&rsquo;ve seldom had to work with the Java Keystore, but as more and more sites start
using SSL, it&rsquo;s grown more important to ensure that certificates stored on your
side don&rsquo;t arbitrarily expire.  This is especially important if you&rsquo;re not
simply relying or verifying intermediate and root certificates.  A self-signed
server certificate is oftentimes <em>easy enough</em> to do for some applications, and
that expiration can come quicker than you anticipate.</p>

<p>In this section, I&rsquo;ll demonstrate a little bit about how you can get at the
certificate store within the relatively standard JKS-format of the Java
Cryptography Keystore.  Other stores may not work this way, or may require
a different set of processes.</p>

<p>Side Note: I have to give credit where credit is due, and <a href="http://stackoverflow.com/a/10986535/7008">this StackOverflow
answer</a>
provided a great starting point for getting things going.  It was essentially
the foundation for what you&rsquo;ll see in this post.</p>

<h1 id="parsing-the-keystore:ea83c6f13264b77e32b6387eb48f3ca8">Parsing the Keystore</h1>

<p>To make this easy, I generally use the following libraries in most of my
projects:</p>

<ol>
<li><a href="https://github.com/google/guava">Google Guava</a></li>
<li><a href="http://www.joda.org/joda-time/">Joda Time</a>

<ul>
<li>Of course, if you&rsquo;re lucky enough to use Java 8, we&rsquo;d be using
<a href="http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html">Java 8&rsquo;s Date and Time</a>
library.  But, for a vast majority of users, we&rsquo;re stuck with 7 or less.
Ah, c&rsquo;est la vie.</li>
</ul></li>
</ol>

<p>Reading the Keystore is actually a pretty straight-forward process:</p>

<pre>
<code class="language-java">
// import a ton of stuff
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.joda.time.DateTime;
import org.joda.time.Days;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

// ... more code, some method somewhere

KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
char[] pass = "changeit"; // or whatever

java.io.InputStream fis = null;
try {
    fis = Reader.class.getClassLoader().getResourceAsStream(file_path);
    ks.load(fis, pass);

    // Get Certificate listing

} finally {
    if (fis != null) {
        fis.close();
    }
}
</code>
</pre>

<h1 id="traversing-certificates-and-aliases:ea83c6f13264b77e32b6387eb48f3ca8">Traversing Certificates and Aliases</h1>

<p>From this point, we&rsquo;re able to read in and claim <strong>success</strong>! Nah, not really.
What do you want to do with this?  Well, in this case, our KeyStore will have
a lot of various certificates and trusts in it.  For our use, we simply want to
validate the ones that are going to expire.</p>

<p>With each certificate that we have, there are likely 1 or more other
certificates in a <a href="https://support.dnsimple.com/articles/what-is-ssl-certificate-chain/"><strong>certificate chain</strong></a>.
We aren&rsquo;t simply going to find expired server certificates (although that&rsquo;s
laudable in and of itself), but we want to be sure we aren&rsquo;t going to suffer the
fate of thinking &ldquo;well, our server cert passed, but this whole tree of servers&rsquo;
intermediate certificates expired&rdquo;.</p>

<pre>
<code class="language-java">
private static Map<String, List<Certificate>> getCertMap(KeyStore ks) throws KeyStoreException {
    Map<String, List<Certificate>> cert_map = Maps.newHashMap(); // Good 'ol Guava
    // Each KeyStore can return an "Enumeration" of aliases
    Enumeration<String> aliases = ks.aliases();
    while (aliases.hasMoreElements()) {
        String alias = aliases.nextElement();
        Certificate[] certs = ks.getCertificateChain(alias);
        // It's not a guarantee that you'll have ANY certificates
        if (certs != null && certs.length > 0) {
            cert_map.put(alias, Lists.newArrayList(certs));
        } else {
            Certificate cert = ks.getCertificate(alias);
            cert_map.put(alias, Lists.newArrayList(cert));
        }
    }
    return cert_map;
}
</code>
</pre>

<h1 id="filtering:ea83c6f13264b77e32b6387eb48f3ca8">Filtering</h1>

<p>Another assumption being made here is that if you have a certificate that&rsquo;s
going to expire, it&rsquo;s most likely an <strong>X.509</strong> certificate, and so we only need
to be concerned with a subset of certificates.</p>

<pre><code class="language-java">
    private static Map<String, List<X509Certificate>> filterX509Certs(Map<String, List<Certificate>> cert_map) {
        Map<String, List<X509Certificate>> x509_map = Maps.newHashMap();
        for (Map.Entry<String, List<Certificate>> c : cert_map.entrySet()) {
            List<X509Certificate> certs = Lists.newArrayListWithCapacity(c.getValue().size());
            for (Certificate cert : c.getValue()) {
                // This is where the magic happens...
                if (cert.getType().equals("X.509")) {
                    // cast the type to X509Certificate
                    certs.add((X509Certificate) cert);
                }
            }
            x509_map.put(c.getKey(), certs);
        }
        return x509_map;
    }
</code></pre>

<p>It took a lot longer to figure out that this one little line</p>

<pre>
<code class="language-java">if (cert.getType().equals("X.509")) {
</code>
</pre>
is where you figure out the type.

# Finding by Date

Now that we have a mapped-listing of X.509 certificates and their chains mapped
to an alias, we can iterate over them to find by date:

<pre>
<code class="language-java">
private static List<X509Certificate> filterCertificates(List<X509Certificate> certs, int days_threshold) {
    List<X509Certificate> filtered = Lists.newArrayList();
    for (X509Certificate cert : certs) {
        DateTime expiration_date = new DateTime(cert.getNotAfter());
        int days_to_expiration = Days.daysBetween(DateTime.now(), expiration_date.getDays();
        if (days_to_expiration < days_threshold) {
            filtered.add(cert);
        }
    }
    return filtered;
}
</code>
</pre>

<p>Here, we&rsquo;re simply making use of the excellent <a href="http://www.joda.org/joda-time/quickstart.html">Joda DateTime</a>
to parse the <code>java.util.Date</code> object and compute the days between the curren
time and the expiration date.  I&rsquo;d rather not worry about leap years, other
calendar types, etc., when computing things, so let&rsquo;s just let a great library
do that for us.</p>

<h1 id="summary:ea83c6f13264b77e32b6387eb48f3ca8">Summary</h1>

<p>I hope this overview of how to parse and compute expiration dates on
certificates in your Keystore can be of some use.  It&rsquo;s not hard, but half the
battle is figuring out <strong>what</strong> you need to know before you do it.</p>

			</section>
		</article>
	</main>


	<footer role="contentinfo">
		<div class="hr"></div>
		<div class="footer-link">
			
			
			
			
		</div>
		<div class="copyright">Copyright &copy; 2016 Nick Klauer</div>
	</footer>

</div>



<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/8.4/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>

</body>
</html>