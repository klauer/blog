+++
date = "2015-10-19T17:16:30-05:00"
draft = true
title = "Leadership - Testing and Training"

+++

This isn't the first time I've discussed leadership, but I feel it comes up
again and again when I see people new to the concept of responsibility and just
having a hell of a time trying to make their way through it's challenges.  This
is especially difficult when you get promoted from a technical position to one
that requires managing a team.

# Military Officer Courses

It should not be a surprise to anyone that the military (and in my specific
case, the United States Armed Forces) employs a very different mentality towards
finding and training leaders than I think almost any other place does.  The
demands of a military officer, be it an Airman, Sailor, Marine, or Soldier are
all based on the understanding that by NOT being able to lead, you can cause
incomparable loss.  Making a poor choice as a Soldier in the field has life or
death consequences.  I can't say that I have ever seen any corporate setting
have nearly that kind of consequence.

All that said, the military officer programs in most branches are designed to
test your decision making skills, push your mind and body to their physical
limits, and expose your weaknesses for all to see.  This is a somewhat brutal
way to learn about yourself, but I cannot think of a better place to do it.
Often during military training exercises, it is said that a mistake on the
training ground is far better than one in the field.  Training lets you **redo**
your mistakes.  The real world is far more harsh.

# Civilian / Corporate Counterparts

All of that preamble should make it apparent that I feel leadership is a sorely
lacking skill in Corporate America.  Businesses live and die by the **people**
that work in them, so being able to lead, is far better than simply *managing*
people.

## Differences between Leadership and Management

