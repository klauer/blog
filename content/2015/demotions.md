+++
date = "2015-10-19T20:28:07-05:00"
draft = true
title = "Demotions"

+++

How often have you seen someone in a leadership or management position get
demoted or reassigned back to a more technical or previous role in an
organization?  How often have you seen someone get let go after being promoted
into a leadership position?  How often do you see someone stay in a position
they're not qualified for?

Personally, of the 3 choices (demotion, firing, staying), I've seen people in
larger organizations stay in their position much, much longer than they probably
should have, or at least longer than they needed to know they were struggling
with the added responsibilities.

Coming from a military background, I have a completely different viewpoint on
how leaders are **made**.  For a lot of people, leadership is not something that
you just "find" yourself into, but something that you work your way up to.  Once
you reach a pinnacle of what a company or organization is able to provide you in
terms of challenges, you tend to find yourself looking at the next set of
challenges being not technical, but social.

A leader both knows his team's technical issues as well as their personal ones.
You're stuck with a lot more on your plate, with little more than a title.  You
have to do things to earn Respect, Trust, Confidence, and all while maintaining
the appearance of composure and competence.

What bothers me the most about promotions is that when we promote someone to
a position of leadership, they seldom have the tools to take on all the added
complexity of interacting with people and the social, and interpersonal aspects
of leading.  I've known too many leaders that fail in one or more of the
following:

* Talking regularly with your team to know what they're doing
* Understanding your team's issues (be it technical or personal)
* Making choices that benefit the team's best interests
* Defending and supporting your team when they're challenged or pressured from
  the outside
* Dealing with the complexity of poor performance, prejudices, various "-isms",
    and training the team to be respectful and open-minded

# Training / Mentorship

This is not just a complex task, but a monumental one.  How many people have
enough exposure with **any** of these to really be able to take the reins of
a leadership position and not stumble with these?

For a lot of cases, I think that there's little chance to be trained on these
kinds of situations, and it tends to be a "Trial By Fire" situation.  Poor life
ahead for the team stuck with a manager having to learn on their feet with these
situations.

So, in many cases, leadership should not hesitate to find out how a new leader
is doing to be able to make the appropriate choices.  A Team Lead's Leader
should be able to determine if his/her team is struggling or thriving, and
provide ample guidance and mentorship in this area.


# Demotion

It's frequently a lack of 
